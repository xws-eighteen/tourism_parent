package com.hyqb.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hyqb.common.entity.Enroll;
import com.hyqb.utils.common.R;

public interface EnrollService extends IService<Enroll> {
    R register(Enroll enroll, String code);

    R sendLoginCode(String phone);

    R login(Enroll enroll);
}
