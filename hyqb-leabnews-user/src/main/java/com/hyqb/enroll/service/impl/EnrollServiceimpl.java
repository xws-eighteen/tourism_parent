package com.hyqb.enroll.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.hyqb.common.entity.Enroll;
import com.hyqb.common.entity.UserDetails;
import com.hyqb.enroll.JwtUtil;
import com.hyqb.enroll.mapper.EnrollMapper;

import com.hyqb.enroll.mapper.UserMapper;
import com.hyqb.enroll.service.EnrollService;
import com.hyqb.utils.common.AliDayuUtils;
import com.hyqb.utils.common.R;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@Service
public class EnrollServiceimpl extends ServiceImpl<EnrollMapper, Enroll> implements EnrollService{

    @Autowired
    EnrollMapper enrollMapper;
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    UserMapper userMapper;
    @Autowired
    JwtUtil jwtUtil;
    //注册
    @Override
    public R register(Enroll enroll, String code) {
        //检查参数
        if (enroll==null){
            //缺少参数
        return R.error("缺少参数");
        }
        if (StringUtils.isEmpty(enroll.getEnrollName())){
            //登录用户名必填
            return  R.error("用户名不能为空");
        }
        if (StringUtils.isEmpty(enroll.getPasword())){
            return  R.error("密码不能为空");
        }
        if (StringUtils.isEmpty(enroll.getPhone())){
            return  R.error("手机号不能为空");
        }
        if (StringUtils.isEmpty(code)){
            //无效参数验证码不能为空
            return  R.error("验证码不能为空");
        }
        //用来判断用户名不能重复还有一个用户给此人有多少手机号也只能注册一一次
        LambdaQueryWrapper<Enroll> query=new LambdaQueryWrapper<>();
        //判断手机号是否注册了
        query.eq(Enroll::getPhone,enroll.getPhone());
        //判断数据库中是否存在此手机号
        Enroll enroll1 = this.baseMapper.selectOne(query);
            if (enroll1!=null){
                //手机号已经被注册
                return R.error("手机号被注册");
            }else {
                //用来判断用户名不能重复还有一个用户给此人有多少手机号也只能注册一一次
                LambdaQueryWrapper<Enroll> usernameno=new LambdaQueryWrapper<>();
                usernameno.eq(Enroll::getEnrollName,enroll.getEnrollName());
                //判断用户名是否重复
                int  count = this.baseMapper.selectCount(usernameno);
                if (count>0){
                    return R.error("用户名存在");
                }else {
                //获取验证
            String redisCode = stringRedisTemplate.opsForValue().get(enroll.getPhone());
            //判断验证码是否过期
            if (!StringUtils.isEmpty(redisCode)){
                //验证码是否正确比较
                if (redisCode.equals(code)){
                    //进行加密
                    String pswd = DigestUtils.md5DigestAsHex((enroll.getPasword()).getBytes());
                    enroll.setPasword(pswd);
                    this.baseMapper.insert(enroll);
                    UserDetails userDetails = new UserDetails();
                    Integer eid=enroll.getId();
                    userDetails.setEid(eid);
                    String name=enroll.getEnrollName();
                    userDetails.setName(name);
                    userDetails.setCreateTime(new Date());
                    this.userMapper.insert(userDetails);
                    return R.success("注册成功");
                }else {
                    return R.error("验证码不对请重新输入");
                }
            }else {
                return  R.error("验证码过期");
            }
                }
            }


    }
//验证码
    @Override
    public R sendLoginCode(String phone) {
        //参数校验
        if (StringUtils.isEmpty(phone)){
            return R.error("参数布能为空");
        }
        //验证装状态书否正常
        AliDayuUtils aliDayuUtils=new AliDayuUtils();
        //随机成生验证码
        int yanzheng = RandomUtil.randomInt(10000, 999999);
        try {
            aliDayuUtils.sendRegisterCode(phone,yanzheng);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //保存在redis中
        stringRedisTemplate.opsForValue().set(phone,yanzheng+"");
        //超时时间
        stringRedisTemplate.expire(phone,5, TimeUnit.MINUTES);
        HashMap hashMap = new HashMap();
        hashMap.put("enroll",yanzheng);
        String jwtToken = jwtUtil.createJWT(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 2), hashMap);
        hashMap.put("token",jwtToken);
        //账号状态正常
        return R.success("发送成功");
    }
//登录
    @Override
    public R login(Enroll enroll) {
        //参数不能为空
        if (enroll==null){
            return  R.error("参数不能为空");
        }
        //用户名不能为空
        if (StringUtils.isEmpty(enroll.getEnrollName())){
            return R.error("用户名不能为空");
        }
        //密码不能为空
        if (StringUtils.isEmpty(enroll.getPasword())){
            return  R.error("密码不能为空");
        }
        LambdaQueryWrapper<Enroll> query=new LambdaQueryWrapper<>();
        query.eq(Enroll::getEnrollName,enroll.getEnrollName());
        //查询数据库中是否有这个用户
        Enroll enroll1 = this.baseMapper.selectOne(query);
        if (enroll1!=null){
            //判断用户封禁还是可用
            if (enroll1.getState().equals("0")){
                return R.error("此用户被封禁");
            }else {
                //首先呢对你输入的密码进行加密
                String pswd = DigestUtils.md5DigestAsHex((enroll.getPasword()).getBytes());
                //密码校验对加密后的密码跟数据库的密码进行比较
                if (enroll1.getPasword().equals(pswd)) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("enroll",enroll1);
                    String jwtToken = jwtUtil.createJWT(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 2), hashMap);
                    hashMap.put("token",jwtToken);
                    System.out.println("您好生成的+"+jwtToken);
                    return R.success(hashMap);
                }else {
                    return R.error("密码不正确请重新输入");
                }
                //判断密码是否正确
            }


        }else {
            return  R.error("用户不存在请重新输入");
        }

    }
}
