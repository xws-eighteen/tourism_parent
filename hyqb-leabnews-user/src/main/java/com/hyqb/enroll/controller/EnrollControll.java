package com.hyqb.enroll.controller;

import com.hyqb.common.entity.Enroll;
import com.hyqb.enroll.EnrollControllerApi;
import com.hyqb.enroll.service.EnrollService;
import com.hyqb.utils.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/enroll")
public class EnrollControll implements EnrollControllerApi {
    @Autowired
    EnrollService enrollService;
//注册
    @RequestMapping("/register")
    @Override
    public R register(@RequestBody Enroll enroll, String code) {
        return enrollService.register(enroll,code);
    }
    //验证码
@RequestMapping("sendLoginCode")
    @Override
    public R sendLoginCode(String phone) throws Exception {
        return enrollService.sendLoginCode(phone);
    }
    //登录
@RequestMapping("/login")
    @Override
    public R login(@RequestBody Enroll enroll) {
        return enrollService.login(enroll);
    }
}
