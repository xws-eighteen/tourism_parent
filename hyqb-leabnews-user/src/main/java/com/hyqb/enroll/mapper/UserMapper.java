package com.hyqb.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyqb.common.entity.UserDetails;

public interface UserMapper extends BaseMapper<UserDetails> {
}
