package com.hyqb.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyqb.common.entity.Enroll;

public interface EnrollMapper extends BaseMapper<Enroll> {
}
