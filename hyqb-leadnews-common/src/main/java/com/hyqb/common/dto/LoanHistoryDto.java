package com.hyqb.common.dto;

import com.hyqb.common.entity.LoanHistory;
import lombok.Data;

@Data
public class LoanHistoryDto extends LoanHistory {
    private String username;
    private String goodsname;

}
