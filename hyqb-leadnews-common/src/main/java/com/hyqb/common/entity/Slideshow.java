package com.hyqb.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author: daiyunzhe
 * @date: 2023年04月06日 16:51
 */
@Data
public class Slideshow {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String slideshow;
}
