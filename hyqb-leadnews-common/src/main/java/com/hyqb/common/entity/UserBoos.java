package com.hyqb.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 幕后老板投资黑云青八项目	
 * </p>
 *
 * @author 想午睡
 * @since 2023-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserBoos implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 打款表id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 幕后公司名称
     */
    private String boosName;

    /**
     * 幕后公司老板名称
     */
    private String boosUsername;

    /**
     * 老板投资黑云青八多少w
     */
    private BigDecimal boosPrice;


}
