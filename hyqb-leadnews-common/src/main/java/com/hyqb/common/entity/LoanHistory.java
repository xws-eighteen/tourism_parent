package com.hyqb.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户贷款详细表
 * </p>
 *
 * @author 想午睡
 * @since 2023-04-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class LoanHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 贷款信息id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer uid;

    private Integer gid;

    /**
     * 用户借款时间
     */
    private LocalDateTime createTime;

    /**
     * 用户借款金额
     */
    private Integer lines;

    /**
     * 用户是否分期0:不分期 1 :3期  2:6期 3:12期
     */
    private Integer stages;

    /**
     * 还款日期
     */
    private LocalDateTime repaymentTime;

    /**
     * 分期的税
     */
    private Double tax;

    /**
     * 1放款成功2未放款 0放款失败
     */
    private String state;


}
