package com.hyqb.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 用户登录注册表
 * </p>
 *
 * @author 想午睡
 * @since 2023-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Enroll implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户登录注册id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户账号名
     */
    private String enrollName;

    /**
     * 账号密码
     */
    private String pasword;

    /**
     * 注册手机号11位
     */
    private String phone;

    /**
     * 账号状态 1封禁 0可用
     */
    private String state;

    /**
     * 账号身份1用户 0管理员
     */
    private String states;


}
