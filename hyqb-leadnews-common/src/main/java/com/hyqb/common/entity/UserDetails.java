package com.hyqb.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 用户详细信息表
 * </p>
 *
 * @author 想午睡
 * @since 2023-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户真实姓名
     */
    private String name;
    private String userName;
    private Integer age;
    private String sex;

    /**
     * 信用分(默认500)
     */
    private String points;

    /**
     * 登录时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     *
     *
     *
     */


    /**
     * (实名认证状态 0未认证 1已认证 2认证失败
     */
    private String states;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 银行卡号 19位
     */
    private String cards;

    /**
     * 身份证号
     */
    private String idNumber;

    /**
     * 用户借款额度默认是5q
     */
    private Integer quota;

    /**
     * 注册人id
     */
    private Integer eid;


}
