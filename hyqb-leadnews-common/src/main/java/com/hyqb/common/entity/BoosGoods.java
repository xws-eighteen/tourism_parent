package com.hyqb.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 想午睡
 * @since 2023-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BoosGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 幕后老板id
     */
    private Integer bid;

    /**
     * 商品id
     */
    private Integer gid;


}
