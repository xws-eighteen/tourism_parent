package com.hyqb.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 商品详细表
 * </p>
 *
 * @author 想午睡
 * @since 2023-04-02
 */
@Data
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 0封禁 1开放
     */
    private String state;

    /**
     * 商品创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 商品修改时间
     */

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 商品可借款额度
     */

    private Integer liness;

    /**
     * 商品详细信息介绍
     */

    private String details;

    /**
     * 商品优点
     */

    private String merits;

    /**
     * 商品游玩地址
     */

    private String address;


    @TableLogic(value = "1", delval = "0")
    @TableField(value = "deletes", fill = FieldFill.INSERT)
    private Integer deletes;


}
