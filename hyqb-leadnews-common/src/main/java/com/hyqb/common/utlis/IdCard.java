package com.hyqb.common.utlis;

import lombok.Data;

@Data
public class IdCard {
    private String code;
    private String message;
    private com.hyqb.common.utlis.Data data;
    private String seqNo;
}
