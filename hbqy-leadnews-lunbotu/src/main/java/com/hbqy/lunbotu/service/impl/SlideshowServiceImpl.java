package com.hbqy.lunbotu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hbqy.lunbotu.mapper.SlideshowMapper;
import com.hbqy.lunbotu.service.SlideshowService;
import com.hyqb.common.entity.Slideshow;
import org.springframework.stereotype.Service;

/**
 * @author: daiyunzhe
 * @date: 2023年04月06日 16:53
 */
@Service
public class SlideshowServiceImpl extends ServiceImpl<SlideshowMapper, Slideshow> implements SlideshowService {
}
