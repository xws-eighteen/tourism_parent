package com.hbqy.lunbotu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hyqb.common.entity.Slideshow;

public interface SlideshowService extends IService<Slideshow> {
}
