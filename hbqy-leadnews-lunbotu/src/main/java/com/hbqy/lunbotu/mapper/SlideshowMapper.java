package com.hbqy.lunbotu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyqb.common.entity.Slideshow;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SlideshowMapper extends BaseMapper<Slideshow> {
}
