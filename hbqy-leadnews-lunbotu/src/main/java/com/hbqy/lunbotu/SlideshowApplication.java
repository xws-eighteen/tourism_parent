package com.hbqy.lunbotu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: daiyunzhe
 * @date: 2023年04月06日 16:49
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.hbqy.lunbotu.mapper")
public class SlideshowApplication {
    public static void main(String[] args) {
        SpringApplication.run(SlideshowApplication.class,args);
    }
}
