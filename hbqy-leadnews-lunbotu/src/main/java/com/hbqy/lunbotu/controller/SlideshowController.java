package com.hbqy.lunbotu.controller;

import com.hbqy.lunbotu.service.SlideshowService;
import com.hyqb.common.entity.Slideshow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: daiyunzhe
 * @date: 2023年04月06日 16:53
 */
@RestController
@RequestMapping("lunbo")
public class SlideshowController {
    @Autowired
    SlideshowService slideshowService;
    @RequestMapping("slideshows")
    public List<Slideshow> slideshowList(){
        return slideshowService.list();
    }
}
