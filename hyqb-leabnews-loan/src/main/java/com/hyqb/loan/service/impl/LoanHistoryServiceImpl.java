package com.hyqb.loan.service.impl;

import com.hyqb.common.dto.LoanHistoryDto;
import com.hyqb.common.entity.LoanHistory;
import com.hyqb.loan.mapper.LoanHistoryMapper;
import com.hyqb.loan.service.LoanHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoanHistoryServiceImpl implements LoanHistoryService {
    @Autowired
    private   LoanHistoryMapper loanHistoryMapper;

    @Override

    public List<LoanHistoryDto> findAll() {
        return  loanHistoryMapper.findAll();
    }
}
