package com.hyqb.loan.service;

import com.hyqb.common.dto.LoanHistoryDto;

import java.util.List;

public interface LoanHistoryService {
    List<LoanHistoryDto> findAll();
}
