package com.hyqb.loan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyqb.common.dto.LoanHistoryDto;
import com.hyqb.common.entity.LoanHistory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface LoanHistoryMapper extends BaseMapper<LoanHistory> {

    List<LoanHistoryDto> findAll();
}
