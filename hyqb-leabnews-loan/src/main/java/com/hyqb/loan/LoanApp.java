package com.hyqb.loan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@MapperScan("com.hyqb.loan.mapper")
@EnableDiscoveryClient
public class LoanApp {
    public static void main(String[] args) {
        SpringApplication.run(LoanApp.class,args);
    }

}
