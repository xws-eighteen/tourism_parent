package com.hyqb.loan.controller;

import com.hyqb.common.dto.LoanHistoryDto;
import com.hyqb.common.entity.LoanHistory;
import com.hyqb.loan.LoanControllerApi;
import com.hyqb.loan.service.LoanHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/loan")
@RestController
public class LoanController implements LoanControllerApi {
   @Autowired
   private  LoanHistoryService loanHistoryService;
@RequestMapping("findAll")
    @Override
    public List<LoanHistoryDto> findAll() {
        return loanHistoryService.findAll();
    }
}
