package com.hyqb.goods.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hyqb.common.entity.Goods;
import com.hyqb.goods.GoodsControllerApi;
import com.hyqb.goods.service.GoodsService;
import com.hyqb.utils.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/goods")
public class GoodsController implements GoodsControllerApi {


    @Autowired
    private GoodsService goodsService;

    @RequestMapping("findPage")
    @Override
    public Page<Goods> findPage(Integer page, Integer pageSize, @RequestBody Goods goods) {
        return goodsService.findPage(page, pageSize, goods);
    }

    @Override
    @RequestMapping("insert")
    public R insertGoods(@RequestBody Goods goods) {
        return goodsService.insert(goods);
    }

    @Override
    @RequestMapping("findById")
    public Goods findByIdGoods(Integer id) {
        return goodsService.findById(id);
    }


    @Override
    @RequestMapping("update")
    public R updateGoods(@RequestBody Goods goods) {
        return goodsService.updateById(goods);
    }

    @Override
    @RequestMapping("delete")
    public R deleteGoods(Integer id) {
        return goodsService.deleteByIdAndInsert(id);
    }

    @RequestMapping("findAll")
    public List<Goods> findAll(){
        return goodsService.findAll();
    }
    @Override
    @RequestMapping("updateState")
    public R updateState(Goods goods) {
        return goodsService.updateGoods(goods);
    }
    @RequestMapping("findHot")
    public List<Goods> findHot(){
        return goodsService.findHot();
    }
}
