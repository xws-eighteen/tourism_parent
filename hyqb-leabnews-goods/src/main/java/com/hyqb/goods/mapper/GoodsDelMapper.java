package com.hyqb.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyqb.common.entity.GoodsDel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GoodsDelMapper extends BaseMapper<GoodsDel> {
}
