package com.hyqb.goods.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hyqb.common.entity.Goods;
import com.hyqb.common.entity.GoodsDel;
import com.hyqb.utils.common.R;

import java.util.List;

public interface GoodsService {


    List<Goods> findAll();

    R insert(Goods goods);

    Goods findById(Integer id);

    R updateById(Goods goods);

    R deleteByIdAndInsert(Integer id);

    Page<Goods> findPage(Integer page, Integer pageSize, Goods goods);

    R updateGoods(Goods goods);

    List<Goods> findHot();

}
