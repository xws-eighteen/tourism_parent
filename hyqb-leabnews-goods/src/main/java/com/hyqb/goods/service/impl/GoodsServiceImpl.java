package com.hyqb.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hyqb.common.entity.Goods;
import com.hyqb.common.entity.GoodsDel;
import com.hyqb.goods.mapper.GoodsDelMapper;
import com.hyqb.goods.mapper.GoodsMapper;
import com.hyqb.goods.service.GoodsService;
import com.hyqb.utils.common.R;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private GoodsDelMapper goodsDelMapper;

    @Override
    public Page<Goods> findPage(Integer page, Integer pageSize, Goods goods) {
        Page<Goods> goodsPage = new Page<>(page, pageSize);
//        LambdaQueryWrapper<Goods> qw = new LambdaQueryWrapper<>();
//        qw.like(Goods::getGoodsName,goods.getGoodsName());
//        qw.like(Goods::getAddress,goods.getAddress());
//        goodsMapper.selectPage(goodsPage,qw);
        goodsMapper.findPage(goods,goodsPage);
        return goodsPage ;

    }

    @Override
    public List<Goods> findAll() {
        return goodsMapper.selectList(null);
    }

    @Override
    public R insert(Goods goods) {
        goodsMapper.insert(goods);
        return R.success("添加成功");
    }

    @Override
    public Goods findById(Integer id) {
        return goodsMapper.selectById(id);
    }

    @Override
    public R updateById(Goods goods) {
        goodsMapper.updateById(goods);
        return R.success("修改成功");
    }

    @Override
    public R deleteByIdAndInsert(Integer id) {
        GoodsDel goodsDel1 = new GoodsDel();
        Goods goods =goodsMapper.selectById(id);
        BeanUtils.copyProperties(goods,goodsDel1);
        goodsDelMapper.insert(goodsDel1);
        goodsMapper.deleteById(goods);
        return R.success("删除成功");
    }
    @Override
    public R updateGoods(Goods goods) {
        goodsMapper.updateById(goods);
        return R.success("修改成功");
    }

    @Override
    public List<Goods> findHot() {
        return goodsMapper.findHot();
    }
}
