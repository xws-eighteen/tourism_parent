package com.hyqb.personal.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyqb.common.entity.UserDetails;

/**
 * <p>
 * 用户详细信息表 Mapper 接口
 * </p>
 *
 * @author 王一鸣
 * @since 2023-04-03
 */
public interface UserDetailsMapper extends BaseMapper<UserDetails> {

    UserDetails findById(Integer eid);
}
