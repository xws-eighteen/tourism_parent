package com.hyqb.personal;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName: PersonalApplication
 * @Author: 王一鸣
 * @date：2023/4/3 16:16
 */
@SpringBootApplication
@MapperScan("com.hyqb.personal.mapper")
@EnableDiscoveryClient
public class PersonalApplication {
    public static void main(String[] args) {
        SpringApplication.run(PersonalApplication.class,args);
    }
}
