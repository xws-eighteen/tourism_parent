package com.hyqb.personal.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.hyqb.common.entity.Goods;
import com.hyqb.common.entity.UserDetails;
import com.hyqb.personal.service.IUserDetailsService;
import com.hyqb.utils.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户详细信息表 控制层
 * </p>
 *
 * @author 王一鸣
 * @since 2023-04-03
 */
@RestController
@RequestMapping("/personal")
public class UserDetailsController {

    @Autowired
    IUserDetailsService userDetailsService;



    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public R addUserDetails(@RequestBody UserDetails userDetails){
        if (Integer.parseInt(userDetails.getPoints())<500){
            return R.error("信誉分太低，无权选择服务！！！");
        }
        userDetailsService.save(userDetails);
        return R.success("成功添加个人信息");
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public R updateUserDetails(@RequestBody UserDetails userDetails){
        if (userDetails.getEid()==null){
            return R.error("未注册账号！！！");
        }
        if (userDetails.getId()==null){
            return R.error("账号不存在！！！");
        }
        userDetailsService.updateById(userDetails);
        return R.success("个人信息更改成功");
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public R deleteOne(@RequestParam Integer id){
        if (null==id) {
            return R.error("用户不存在!!!");
        }
        userDetailsService.removeById(id);
        return R.success("已删除个人信息");
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        userDetailsService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public UserDetails findById(@RequestParam Integer eid){
        return userDetailsService.findById(eid);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<UserDetails> findAll(){
        return userDetailsService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<UserDetails> findPage(@RequestParam Integer page,@RequestParam Integer pageSize,@RequestBody UserDetails userDetails){
        LambdaQueryWrapper<UserDetails> wrapper = new LambdaQueryWrapper<>();
//        用户id精准查询
        wrapper.eq(userDetails.getId()!=null,UserDetails::getId,userDetails.getId());
//        用户真实姓名查询
        wrapper.like(userDetails.getName()!=null,UserDetails::getName,userDetails.getName());
//        用户住址查询
        wrapper.like(userDetails.getAddress()!=null,UserDetails::getAddress,userDetails.getAddress());
//        用户状态查询
        wrapper.eq(userDetails.getStates()!=null,UserDetails::getStates,userDetails.getStates());
        Page<UserDetails> userDetailsPage = new Page<>(page, pageSize);
        return userDetailsService.page(userDetailsPage,wrapper);
    }


}
