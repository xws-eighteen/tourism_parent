package com.hyqb.personal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.hyqb.common.entity.UserDetails;
import com.hyqb.personal.mapper.UserDetailsMapper;
import com.hyqb.personal.service.IUserDetailsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 用户详细信息表 服务实现类
 * </p>
 *
 * @author 王一鸣
 * @since 2023-04-03
 */
@Service
public class UserDetailsServiceImpl extends ServiceImpl<UserDetailsMapper, UserDetails> implements IUserDetailsService {
    @Resource
    UserDetailsMapper userDetailsMapper;
    @Override
    public UserDetails findById(Integer eid) {
        return userDetailsMapper.findById(eid);
    }

}
