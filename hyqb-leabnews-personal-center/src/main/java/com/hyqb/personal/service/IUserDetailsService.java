package com.hyqb.personal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hyqb.common.entity.UserDetails;

/**
 * <p>
 * 用户详细信息表 服务类
 * </p>
 *
 * @author 王一鸣
 * @since 2023-04-03
 */
public interface IUserDetailsService extends IService<UserDetails> {

    UserDetails findById(Integer eid);
}
