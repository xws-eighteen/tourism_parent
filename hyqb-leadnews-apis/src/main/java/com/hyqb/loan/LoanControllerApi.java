package com.hyqb.loan;

import com.hyqb.common.dto.LoanHistoryDto;

import java.util.List;

public interface LoanControllerApi {
public List<LoanHistoryDto> findAll();

}
