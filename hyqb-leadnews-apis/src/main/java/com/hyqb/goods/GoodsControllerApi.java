package com.hyqb.goods;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hyqb.common.entity.Goods;
import com.hyqb.utils.common.R;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 当前时间2023/4/6/21:03
 * 这是想午睡给你创建的一个页面
 */
public interface GoodsControllerApi {
    public Page<Goods> findPage(Integer page, Integer pageSize,Goods goods);

    public R insertGoods(Goods goods);

    public Goods findByIdGoods(Integer id);

    public R updateGoods(Goods goods);

    public R deleteGoods(Integer id);
    public R updateState(@RequestBody Goods goods);
}
