package com.hyqb.enroll;

import com.hyqb.common.entity.Enroll;
import com.hyqb.utils.common.R;

public interface EnrollControllerApi {
    //注册
    public R register(Enroll enroll, String ss);

//    //发送注册短信验证码
    public R sendLoginCode(String phone) throws Exception;

    //登录
    public R login(Enroll enroll);
}
