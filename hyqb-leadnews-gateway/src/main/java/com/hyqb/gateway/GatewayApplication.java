package com.hyqb.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 当前时间2023/3/21/16:52
 * 这是想午睡给你创建的一个页面
 */
@SpringBootApplication
@EnableDiscoveryClient  //开启注册中心
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class,args);
    }
}