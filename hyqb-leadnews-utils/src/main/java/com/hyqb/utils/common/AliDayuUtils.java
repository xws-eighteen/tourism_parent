package com.hyqb.utils.common;


import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.hyqb.utils.test.Sample;

/**
 * 文件名: AliDayuUtils
 * 创建者: 仵老师
 * 创建时间:2022/6/28 15:28
 * 描述: 这是一个示例
 */
public class AliDayuUtils {

    /**
     *
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    public void sendRegisterCode(String phone,int code)  throws Exception{
        com.aliyun.dysmsapi20170525.Client client = Sample.createClient("LTAIlzwM0t5b0i4r", "moYvfwB0g0xQLygciSMjXPYQDXapnj");
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setPhoneNumbers(phone)
                .setSignName("仵志松个人网站")
                .setTemplateCode("SMS_138550047")
                .setTemplateParam("{\"code\":\""+code+"\"}");
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        SendSmsResponse sendSmsResponse=client.sendSmsWithOptions(sendSmsRequest,runtime);
        System.out.println(sendSmsResponse.getBody().getCode());

    }
}